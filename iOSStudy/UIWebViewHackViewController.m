//
//  UIWebViewHackViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/18.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "UIWebViewHackViewController.h"

@interface UIWebViewHackViewController ()

@property UIWebView *webView;
@property UISwitch *bounce;
@property UISwitch *scroll;

@end

@implementation UIWebViewHackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _webView = [[UIWebView alloc] init];
    [_webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height/2) ];
    [self.view addSubview:_webView];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.google.com"]];
    [_webView loadRequest:request];
    
    // Switch
    _bounce = [[UISwitch alloc] init];
    _scroll = [[UISwitch alloc] init];
    
    [self.view addSubview:_bounce];
    [self.view addSubview:_scroll];
    
    [_bounce setFrame:CGRectMake(((self.view.frame.size.width/2)-100)/2, (self.view.frame.size.height/2) + ((self.view.frame.size.height/2)/2), 100, 30)];
    [_scroll setFrame:CGRectMake((((self.view.frame.size.width/2)-100)/2)+(self.view.frame.size.width/2), (self.view.frame.size.height/2) + ((self.view.frame.size.height/2)/2), 100, 30)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
