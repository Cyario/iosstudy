//
//  NSNotificationCenterViewController.m
//  iOSStudy
//

#import "NSNotificationCenterViewController.h"

@interface NSNotificationCenterViewController ()

@property UILabel       *label;
@property NSMutableDictionary  *object;

@end

@implementation NSNotificationCenterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Object Initialize
    self.object = @{@"value": @0}.mutableCopy;
    
    // Label
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 30)];
    [self.label setTextAlignment:NSTextAlignmentCenter];
    [self.label setText:@"..."];
    [self.view addSubview:self.label];
    
    // Buton
	UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	button.frame = CGRectMake( (self.view.frame.size.width-100)/2, 200, 100, 30 );
	[button setTitle:@"TAP" forState:UIControlStateNormal];
	[button addTarget:self action:@selector(onButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    // 受信
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];

    // System Notification
    // [nc addObserver:self selector:@selector(notificationCallBack:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    // Callback
    // [nc addObserver:self selector:@selector(notificationCallBack:) name:@"OriginalNotification" object:nil];
    
    // Block
    [nc addObserverForName:@"OriginalNotification" object:nil queue:nil usingBlock:^(NSNotification *notification){
        
        [self.label setText:[NSString stringWithFormat:@"value = %@", [[notification userInfo] objectForKey:@"value"]]];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

// Callback
-(void) notificationCallBack:(NSNotificationCenter*)center {
    //NSString *value = [[center userInfo] objectForKey:@"KEY"];
    NSLog(@"Recieved the notification!");
}

- (void)onButtonTapped: (id)sender
{
    // 送信
    int value = [[self.object objectForKey:@"value"] intValue];
    [self.object setObject:@(++value) forKey:@"value"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OriginalNotification" object:self userInfo:self.object];
}

@end
