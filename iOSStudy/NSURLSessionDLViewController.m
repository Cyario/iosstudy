//
//  NSURLSessionDLViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/10.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "NSURLSessionDLViewController.h"

@interface NSURLSessionDLViewController ()

@end

@implementation NSURLSessionDLViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    NSString *urlString = @"http://api.openweathermap.org/data/2.5/weather?q=Tokyo";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionDownloadTask *sessionDownloadTask = [session downloadTaskWithURL:url];
    [sessionDownloadTask resume];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    
    NSLog(@"[bytesWritten] %lld, [totalBytesWritten] %lld, [totalBytesExpectedToWrite] %lld", bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
    double progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    NSLog(@"[progress] %f", progress);
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    
    if( [location isFileURL] ){
        NSLog(@"[location absoluteString] %@", [location absoluteString]);
        
        NSNumber *fileSizeBytes;
        [location getResourceValue:&fileSizeBytes forKey:NSURLFileSizeKey error:nil];
        NSLog(@"[fileSizeBytes] %@", fileSizeBytes);
        
        // 実際にダウンロードされたファイルのパスは下記のような感じになります．
        // ダウンロードしたファイルはパーマネントな場所に移します．
        // file:///private/var/mobile/Applications/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/tmp/CFNetworkDownload_xxxxxx.tmp
        
        //NSFileManager *fileManager = [[NSFileManager alloc] init];
        //[fileManager copy
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if(error){
        NSLog(@"%@", [error localizedDescription]);
    }else{
        NSLog(@"done!");
    }
}

@end
