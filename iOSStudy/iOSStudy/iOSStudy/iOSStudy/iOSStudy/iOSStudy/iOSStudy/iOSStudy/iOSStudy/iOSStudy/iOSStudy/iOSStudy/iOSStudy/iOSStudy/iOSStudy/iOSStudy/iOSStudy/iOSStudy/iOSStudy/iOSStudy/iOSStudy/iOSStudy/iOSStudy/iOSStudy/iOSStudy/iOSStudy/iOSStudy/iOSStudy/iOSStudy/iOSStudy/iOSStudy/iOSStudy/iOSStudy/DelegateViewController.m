//
//  DelegateViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/07.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "DelegateViewController.h"

@interface DelegateViewController ()

@property int value;
@property UILabel   *label;
@property DelegateClass *delegateClass;

@end

@implementation DelegateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    self.value = 0;
    self.delegateClass = [[DelegateClass alloc] init];
    self.delegateClass.delegate = self;
    
    // Label
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 30)];
    [self.label setTextAlignment:NSTextAlignmentCenter];
    [self.label setText:@"..."];
    [self.view addSubview:self.label];
    
    // Buton
	UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	button.frame = CGRectMake( (self.view.frame.size.width-100)/2, 200, 100, 30 );
	[button setTitle:@"TAP" forState:UIControlStateNormal];
	[button addTarget:self action:@selector(onButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) onButtonTapped:(id) sender{
    self.value++;
    [self.delegateClass someEventOccurred];
}

- (void) testDelegate {
    [self.label setText:[NSString stringWithFormat:@"value = %d", self.value]];
}

@end
