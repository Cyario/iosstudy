//
//  DelegateClass.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/07.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "DelegateClass.h"

@interface DelegateClass ()

@end

@implementation DelegateClass

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)someEventOccurred
{
    if ( [self.delegate respondsToSelector:@selector(testDelegate)] ) {
        [self.delegate testDelegate];
    }
}

@end
