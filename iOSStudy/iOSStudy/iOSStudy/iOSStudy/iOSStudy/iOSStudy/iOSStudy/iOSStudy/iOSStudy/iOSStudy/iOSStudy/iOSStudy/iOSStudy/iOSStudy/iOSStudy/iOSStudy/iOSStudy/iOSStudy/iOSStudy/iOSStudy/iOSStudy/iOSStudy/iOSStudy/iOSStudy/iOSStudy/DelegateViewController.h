//
//  DelegateViewController.h
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/07.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DelegateClass.h"

@interface DelegateViewController : UIViewController<TestDelegate>

@end
