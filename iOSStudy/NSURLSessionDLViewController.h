//
//  NSURLSessionDLViewController.h
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/10.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSURLSessionDLViewController : UIViewController<NSURLSessionDataDelegate>

@end
