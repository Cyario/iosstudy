//
//  TaskCompletionViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/10.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "TaskCompletionViewController.h"

@interface TaskCompletionViewController ()

@end

@implementation TaskCompletionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
