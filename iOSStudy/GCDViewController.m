//
//  GCDViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/08.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "GCDViewController.h"

@interface GCDViewController () {
    UILabel *label;
    UIButton *buttonAsync;
    UIButton *buttonSync;
}
@end

@implementation GCDViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // label
    label = [[UILabel alloc] initWithFrame:CGRectMake(155, 100, 100, 250)];
    label.numberOfLines = 11;
    [self.view addSubview:label];
    
    // Buton
	buttonAsync = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	buttonAsync.frame = CGRectMake( ((self.view.frame.size.width/2)-100)/2, 350, 100, 30 );
	[buttonAsync setTitle:@"Async" forState:UIControlStateNormal];
	[buttonAsync addTarget:self action:@selector(onAsyncButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonAsync];
    
	buttonSync = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	buttonSync.frame = CGRectMake( (self.view.frame.size.width/2) + ((self.view.frame.size.width/2)-100)/2, 350, 100, 30 );
	[buttonSync setTitle:@"Sync" forState:UIControlStateNormal];
	[buttonSync addTarget:self action:@selector(onSyncButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonSync];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) onAsyncButtonTapped:(id) sender {
    
    [buttonAsync setEnabled:NO];
    [buttonSync  setEnabled:NO];
    [label setText:@""];
     
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    
    for (int i = 0; i < 10; i++) {
        dispatch_group_async(group, queue, ^{
            
            for ( unsigned long long i = 0; i < 200000000; i++ );
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *str = (label.text) ? label.text : @"";
                
                [label setText: [NSString stringWithFormat:@"%@\n%d", str, i]];
            });
            
        });
    }
    
    dispatch_group_notify(group, queue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [buttonAsync setEnabled:YES];
            [buttonSync  setEnabled:YES];
        });
    });
    
    // dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    
}

- (void) onSyncButtonTapped:(id) sender {
    
    [label setText:@""];

    for ( int i = 0; i < 10; i++) {
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

            for ( unsigned long long i = 0; i < 200000000; i++ );

            if ( [NSThread currentThread].isMainThread ) {
                NSString *str = (label.text) ? label.text : @"";
                [label setText: [NSString stringWithFormat:@"%@\n%d", str, i]];
                
            } else {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    NSString *str = (label.text) ? label.text : @"";
                    [label setText: [NSString stringWithFormat:@"%@\n%d", str, i]];
                });
            }
            
        });
    }
}

@end
