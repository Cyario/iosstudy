//
//  AsynchronousRequestViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/08.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "AsynchronousRequestViewController.h"

@interface AsynchronousRequestViewController ()

@property UILabel           *label;
@property UIButton          *button;

@end

@implementation AsynchronousRequestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Label
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 30)];
    [self.label setTextAlignment:NSTextAlignmentCenter];
    [self.label setText:@"..."];
    [self.view addSubview:self.label];
    
    // Buton
	self.button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	self.button.frame = CGRectMake( (self.view.frame.size.width-100)/2, 200, 100, 30 );
	[self.button setTitle:@"TAP" forState:UIControlStateNormal];
	[self.button addTarget:self action:@selector(onButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

// ボタンタップ時のコールバック
- (void)onButtonTapped: (id)sender
{
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSURL *url = [NSURL URLWithString:@"http://api.openweathermap.org/data/2.5/weather?q=Tokyo"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    dispatch_semaphore_t s = dispatch_semaphore_create(0);
    
    [NSURLConnection sendAsynchronousRequest: request
                                       queue: queue
                           completionHandler: ^(NSURLResponse *res, NSData *data, NSError *error) {
                               
                               NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                               NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
                               NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
                               NSAssert([NSJSONSerialization isValidJSONObject:jsonDict], @"Invalid JSON");
                               
                               dispatch_async(dispatch_get_main_queue(), ^(void){
                                   [self.label setText:jsonDict[@"weather"][0][@"description"]];
                                   [self.button setEnabled:YES];
                               });
                               
                               dispatch_semaphore_signal(s);
                               
                           }];
    
    dispatch_semaphore_wait(s, DISPATCH_TIME_FOREVER);
    
    [self.button setEnabled:NO];
}

@end
