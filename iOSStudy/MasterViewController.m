//
//  MasterViewController.m
//  iOSStudy
//

#import "MasterViewController.h"
#import "DetailViewController.h"

#define kItemKeyTitle       @"title"
#define kItemKeyDescription @"description"
#define kItemKeyClassPrefix @"prefix"

typedef NS_ENUM(NSInteger, SECTION_NAME) {
    NOTIFICATION = 0,
    EVENTS,
    NETWORK,
    MULTITASK,
    BACKGROUND,
    HACK,
    
    SECTION_NUM,
};

@interface MasterViewController () {
}

@property (nonatomic, strong) NSArray *noticeList;
@property (nonatomic, strong) NSArray *eventList;
@property (nonatomic, strong) NSArray *networkList;
@property (nonatomic, strong) NSArray *multiTaskList;
@property (nonatomic, strong) NSArray *backgroundList;
@property (nonatomic, strong) NSArray *hackList;

@end


@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Notification
    self.noticeList = @[
                       
                // LocalNotification
                @{kItemKeyTitle:         @"LocalNotification",
                  kItemKeyDescription:   @"LocalNotification Sample",
                  kItemKeyClassPrefix:   @"LocalNotification",
                },
    ];
                       
    // events
    self.eventList = @[

               // Delegate
               @{kItemKeyTitle:         @"Delegate",
                 kItemKeyDescription:   @"Delegate Sample",
                 kItemKeyClassPrefix:   @"Delegate",
               },
               
               // KVO
               @{kItemKeyTitle:         @"KVO",
                 kItemKeyDescription:   @"Key Value Observing Sample",
                 kItemKeyClassPrefix:   @"KVO",
               },
               
               // NSNotificationCenter
               @{kItemKeyTitle:         @"NSNotificationCenter",
                 kItemKeyDescription:   @"NSNotificationCenter Sample",
                 kItemKeyClassPrefix:   @"NSNotificationCenter",
               },
    ];

    // network
    self.networkList = @[
                       
               // NSURLConnection
               @{kItemKeyTitle:         @"NSURLConnection",
                 kItemKeyDescription:   @"NSURLConnection Sample",
                 kItemKeyClassPrefix:   @"NSURLConnection",
               },
               
               // NSURLConnection(Runloop Async)
               @{kItemKeyTitle:         @"Runloop Async",
                 kItemKeyDescription:   @"NSURLConnection Runloop Async Sample",
                 kItemKeyClassPrefix:   @"RunloopAsync",
               },
               
               // NSURLConnection(sendAsynchronousRequest)
               @{kItemKeyTitle:         @"AsynchronousRequest",
                 kItemKeyDescription:   @"NSURLConnection sendAsynchronousRequest Sample",
                 kItemKeyClassPrefix:   @"AsynchronousRequest",
               },
               
               // NSURLSession
               @{kItemKeyTitle:         @"NSURLSession",
                 kItemKeyDescription:   @"NSURLSession Sample",
                 kItemKeyClassPrefix:   @"NSURLSession",
               },
               
               // NSURLSession Download
               @{kItemKeyTitle:         @"NSURLSession Download",
                 kItemKeyDescription:   @"NSURLSession Download Sample",
                 kItemKeyClassPrefix:   @"NSURLSessionDL",
               },
    ];
    
    // Multi Task
    self.multiTaskList = @[
               // NSThread
               @{kItemKeyTitle:         @"NSThread",
                 kItemKeyDescription:   @"NSThread Sample",
                 kItemKeyClassPrefix:   @"NSThread",
               },
               
               // Grand Central Dispatch
               @{kItemKeyTitle:         @"GCD",
                 kItemKeyDescription:   @"Grand Central Dispatch Sample",
                 kItemKeyClassPrefix:   @"GCD",
               },
    ];
    
    // Background
    self.multiTaskList = @[
                           
                // Task Completion
                @{kItemKeyTitle:         @"TaskCompletion",
                 kItemKeyDescription:    @"TaskCompletion Sample",
                 kItemKeyClassPrefix:    @"TaskCompletion",
                },
                
                // BackgroundFetch
                @{kItemKeyTitle:         @"BackgroundFetch",
                  kItemKeyDescription:   @"BackgroundFetch Sample",
                  kItemKeyClassPrefix:   @"BackgroundFetch",
                },
    ];
    
    // Hack
    self.hackList = @[
                           
                // UIWebView Hack
                @{kItemKeyTitle:            @"UIWebView Hack",
                    kItemKeyDescription:    @"bounce off & scroll off",
                    kItemKeyClassPrefix:    @"UIWebViewHack",
                },
                
                // UIWebView Hack
                @{kItemKeyTitle:          @"Mosaic Effect",
                  kItemKeyDescription:    @"Mosaic Effect",
                  kItemKeyClassPrefix:    @"UIMosaic",
                },
    ];

}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Needed after custome transition
    self.navigationController.delegate = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

// =============================================================================
#pragma mark - UITableViewDataSource

// セクション数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return SECTION_NUM;
}

// セクションタイトル
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    NSString *title = @"";
    
    switch(section) {
        case NOTIFICATION:
            title = @"Notification";
            break;
        case EVENTS:
            title = @"Events";
            break;
        case NETWORK:
            title = @"Network";
            break;
        case MULTITASK:
            title = @"MultiTask";
            break;
        case BACKGROUND:
            title = @"Background";
            break;
        case HACK:
            title = @"Hack";
            break;
    }
    
    return title;
}

// カラム数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSNumber *columns = @0;
    
    switch(section) {
        case NOTIFICATION:
            columns = @([self.noticeList count]);
            break;
        case EVENTS:
            columns = @([self.eventList count]);
            break;
        case NETWORK:
            columns = @([self.networkList count]);
            break;
        case MULTITASK:
            columns = @([self.multiTaskList count]);
            break;
        case BACKGROUND:
            columns = @([self.backgroundList count]);
            break;
        case HACK:
            columns = @([self.hackList count]);
            break;
    }
    
    return columns.intValue;
}

// セルの表示物
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.textColor = [UIColor colorWithRed:51./255. green:153./255. blue:204./255. alpha:1.0];
        cell.detailTextLabel.numberOfLines = 0;
    }
    
    NSDictionary *info = @{};
    switch( indexPath.section ) {
        case NOTIFICATION:
            info = [self.noticeList objectAtIndex:indexPath.row];
            cell.textLabel.text         = info[kItemKeyTitle];
            cell.detailTextLabel.text   = info[kItemKeyDescription];
            break;
        case EVENTS:
            info = [self.eventList objectAtIndex:indexPath.row];
            cell.textLabel.text         = info[kItemKeyTitle];
            cell.detailTextLabel.text   = info[kItemKeyDescription];
            break;
        case NETWORK:
            info = [self.networkList objectAtIndex:indexPath.row];
            cell.textLabel.text         = info[kItemKeyTitle];
            cell.detailTextLabel.text   = info[kItemKeyDescription];
            break;
        case MULTITASK:
            info = [self.multiTaskList objectAtIndex:indexPath.row];
            cell.textLabel.text         = info[kItemKeyTitle];
            cell.detailTextLabel.text   = info[kItemKeyDescription];
            break;
        case BACKGROUND:
            info = [self.backgroundList objectAtIndex:indexPath.row];
            cell.textLabel.text         = info[kItemKeyTitle];
            cell.detailTextLabel.text   = info[kItemKeyDescription];
            break;
        case HACK:
            info = [self.hackList objectAtIndex:indexPath.row];
            cell.textLabel.text         = info[kItemKeyTitle];
            cell.detailTextLabel.text   = info[kItemKeyDescription];
            break;
    }
    
    return cell;
}


// =============================================================================
#pragma mark - UITableViewDelegate

// セルの高さ
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50.0;
}

// セルが選択された時
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *item  = @{};
    NSString *className = @"";
    
    switch( indexPath.section ) {
        case NOTIFICATION:
            item = self.noticeList[indexPath.row];
            break;
        case EVENTS:
            item = self.eventList[indexPath.row];
            break;
        case NETWORK:
            item = self.networkList[indexPath.row];
            break;
        case MULTITASK:
            item = self.multiTaskList[indexPath.row];
            break;
        case BACKGROUND:
            item = self.backgroundList[indexPath.row];
            break;
        case HACK:
            item = self.hackList[indexPath.row];
            break;
    }
    className = [item[kItemKeyClassPrefix] stringByAppendingString:@"ViewController"];
    
    if (NSClassFromString(className)) {
        
        Class aClass = NSClassFromString(className);
        id instance = [[aClass alloc] init];
        
        if ( [instance isKindOfClass:[UIViewController class]]) {
            
            [(UIViewController *)instance setTitle:item[kItemKeyTitle]];
            [self.navigationController pushViewController:(UIViewController *)instance animated:YES];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
