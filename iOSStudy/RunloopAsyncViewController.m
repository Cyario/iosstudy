//
//  RunloopAsyncViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/08.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "RunloopAsyncViewController.h"

@interface RunloopAsyncViewController () {
    NSMutableData   *receivedData;
    NSURLConnection *conn;
    long long expectedDataLength;
}

@property UILabel           *label;
@property UIProgressView    *progress;
@property UIButton          *button;

@end

@implementation RunloopAsyncViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Progress Bar
    self.progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    self.progress.frame = CGRectMake((self.view.frame.size.width-200)/2, 300, 200, 10);
    self.progress.progress = 0.0;
    [self.view addSubview:self.progress];
    
    // Label
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 30)];
    [self.label setTextAlignment:NSTextAlignmentCenter];
    [self.label setText:@"..."];
    [self.view addSubview:self.label];
    
    // Buton
	self.button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	self.button.frame = CGRectMake( (self.view.frame.size.width-100)/2, 200, 100, 30 );
	[self.button setTitle:@"TAP" forState:UIControlStateNormal];
	[self.button addTarget:self action:@selector(onButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button];
    
    // init
    receivedData = [[NSMutableData alloc] initWithCapacity:0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

// レスポンスを受け取った時点で呼び出される
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"didReceiveResponse");
    
    [receivedData setLength:0];
    
    NSInteger statusCode = [((NSHTTPURLResponse *)response) statusCode];
    
    if ( statusCode == 200 ) {
        expectedDataLength = [response expectedContentLength];
    } else {
        [self.button setEnabled:YES];
    }
}

// データを受け取る度に呼び出される
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"didReceiveData");
    
    self.progress.progress = [data length] / expectedDataLength;
    
    [receivedData appendData:data];
}

// データを全て受け取ると呼び出される
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"connectionDidFinishLoading");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *json = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        
        NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSAssert([NSJSONSerialization isValidJSONObject:jsonDict], @"Invalid JSON");
        [self.label setText:jsonDict[@"weather"][0][@"description"]];
    });
    
    [self.button setEnabled:YES];
    [conn unscheduleFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

// ボタンタップ時のコールバック
- (void)onButtonTapped: (id)sender
{
    NSString *url = @"http://api.openweathermap.org/data/2.5/weather?q=Tokyo"; // weather focus API
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    [conn scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    
    [self.button setEnabled:NO];
    self.progress.progress = 0.0;
    [conn start];
}

@end
