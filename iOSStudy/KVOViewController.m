//
//  KVOViewController.m
//  iOSStudy
//

#import "KVOViewController.h"

@interface KVOViewController ()

@property UIView    *kvoObject;
@property UILabel   *label;

@end

@implementation KVOViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Label
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 30)];
    [self.label setTextAlignment:NSTextAlignmentCenter];
    [self.label setText:@"..."];
    [self.view addSubview:self.label];
    
    // Buton
	UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	button.frame = CGRectMake( (self.view.frame.size.width-100)/2, 200, 100, 30 );
	[button setTitle:@"TAP" forState:UIControlStateNormal];
	[button addTarget:self action:@selector(onButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    // KVO
    self.kvoObject = [[UIView alloc] init];
    self.kvoObject.tag = 0;
    [self.kvoObject addObserver:self forKeyPath:@"tag" options:0 context:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)onButtonTapped: (id)sender
{
   self.kvoObject.tag++;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self.label setText:[NSString stringWithFormat:@"tag = %lu", self.kvoObject.tag]];
    // NSLog(@"keyPath=%@\nobject=%@\nchange=%@\ncontext=%@", keyPath, object, change, context);
}

@end
