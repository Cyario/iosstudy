//
//  UIMosaicViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/11/01.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "UIMosaicViewController.h"

@interface UIMosaicViewController ()

@end

@implementation UIMosaicViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *bg = [UIImage imageNamed:@"image.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:bg];
    [self.view addSubview:imageView];
    
    imageView.layer.shouldRasterize = YES;
    imageView.layer.rasterizationScale = 0.5;
    imageView.layer.minificationFilter = kCAFilterTrilinear;
    imageView.layer.magnificationFilter= kCAFilterNearest;
    
    __block NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:[NSBlockOperation blockOperationWithBlock:^{
        imageView.layer.rasterizationScale -= 0.01;
        if ( imageView.layer.rasterizationScale <= 0.f ) {
            [timer invalidate];
        }
    }] selector:@selector(main) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
