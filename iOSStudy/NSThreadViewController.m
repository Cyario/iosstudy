//
//  NSThreadViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/08.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "NSThreadViewController.h"

@interface NSThreadViewController () {
    UILabel *label1;
    UILabel *label1Task;
    UILabel *label2;
    UILabel *label2Task;
    UIButton * button;
    UIActivityIndicatorView *indicator1;
    UIActivityIndicatorView *indicator2;
}

@end

@implementation NSThreadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Buton
	button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	button.frame = CGRectMake( (self.view.frame.size.width-100)/2, 350, 100, 30 );
	[button setTitle:@"TAP" forState:UIControlStateNormal];
	[button addTarget:self action:@selector(onButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    // Label
    label1     = [[UILabel alloc] initWithFrame:CGRectMake(70, 100, 100, 30)];
    label2     = [[UILabel alloc] initWithFrame:CGRectMake(70, 150, 100, 30)];
    label1Task = [[UILabel alloc] initWithFrame:CGRectMake(170, 100, 100, 30)];
    label2Task = [[UILabel alloc] initWithFrame:CGRectMake(170, 150, 100, 30)];
    
    [self.view addSubview:label1];
    [self.view addSubview:label1Task];
    [self.view addSubview:label2];
    [self.view addSubview:label2Task];
    
    // indicator
    indicator1 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator1 setFrame:CGRectMake(80,  155, indicator1.frame.size.width, indicator1.frame.size.height)];
    indicator2 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator2 setFrame:CGRectMake(180, 155, indicator2.frame.size.width, indicator2.frame.size.height)];
    [self.view addSubview:indicator1];
    [self.view addSubview:indicator2];
     
    [label1 setText:@"MainThread:"];
    [label2 setText:@"SubThread:"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) subThread {
    
    for ( unsigned long long i = 0; i < 2000000000; i++ );
    [label2Task setText:@"Done!!"];
    [button setEnabled:YES];
    [indicator2 stopAnimating];
}

- (void) onButtonTapped:(id) sender {
    
    [button setEnabled:NO];
    [label1Task setText:@""];
    [label2Task setText:@""];
    
    [indicator1 startAnimating];
    [indicator2 startAnimating];
    [NSThread detachNewThreadSelector: @selector(subThread)
                             toTarget: self
                           withObject: nil];
    
    [label1Task setText:@"Done!!"];
    [indicator1 stopAnimating];
}

@end
