//
//  LocalNotificationViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/10.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "LocalNotificationViewController.h"

@interface LocalNotificationViewController ()

@property UIButton  *button;

@end

@implementation LocalNotificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Buton
	self.button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	self.button.frame = CGRectMake( (self.view.frame.size.width-100)/2, 350, 100, 30 );
	[self.button setTitle:@"TAP" forState:UIControlStateNormal];
	[self.button addTarget:self action:@selector(onButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) onButtonTapped:(id) sender {
    
    UILocalNotification *ln = [[UILocalNotification alloc] init];
    ln.fireDate = [[NSDate date] dateByAddingTimeInterval:10];
    ln.timeZone = [NSTimeZone defaultTimeZone];
    
    ln.alertBody = [NSString stringWithFormat:@"Hello"];
    ln.alertAction = @"Open";
    
    ln.soundName = UILocalNotificationDefaultSoundName;
    ln.applicationIconBadgeNumber = 1;
    
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"Hello" forKey:@"EventKey"];
    ln.userInfo = infoDict;
    [[UIApplication sharedApplication] scheduleLocalNotification:ln];
    
}

@end
