//
//  NSURLSessionViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/07.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "NSURLSessionViewController.h"

@interface NSURLSessionViewController ()

@end

@implementation NSURLSessionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    NSLog(@"%@", [[NSBundle mainBundle] bundlePath]);

    // Request
    NSURL *url = [NSURL URLWithString:@"http://api.openweathermap.org/data/2.5/weather?q=Tokyo"];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate:self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithURL: url completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
                                        if ( error == nil ) {
                                            NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                            NSLog(@"Data = %@",text);
                                        }
                                    }];
    [dataTask resume];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
