//
//  NSURLConnectionViewController.m
//  iOSStudy
//
//  Created by 大村 徹 on 2013/10/07.
//  Copyright (c) 2013年 Toru Omura. All rights reserved.
//

#import "NSURLConnectionViewController.h"

@interface NSURLConnectionViewController ()

@property UILabel           *label;
@property UIButton          *button;

@end

@implementation NSURLConnectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Label
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 30)];
    [self.label setTextAlignment:NSTextAlignmentCenter];
    [self.label setText:@"..."];
    [self.view addSubview:self.label];
    
    // Buton
	self.button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	self.button.frame = CGRectMake( (self.view.frame.size.width-100)/2, 200, 100, 30 );
	[self.button setTitle:@"TAP" forState:UIControlStateNormal];
	[self.button addTarget:self action:@selector(onButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

// ボタンタップ時のコールバック
- (void)onButtonTapped: (id)sender
{
    
    NSURL *url = [NSURL URLWithString:@"http://api.openweathermap.org/data/2.5/weather?q=Tokyo"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *requestData = [
                    NSURLConnection
                    sendSynchronousRequest : request
                    returningResponse : &response
                    error : &error
                ];
    
    NSString *json = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSAssert([NSJSONSerialization isValidJSONObject:jsonDict], @"Invalid JSON");
    [self.label setText:jsonDict[@"weather"][0][@"description"]];
}

@end